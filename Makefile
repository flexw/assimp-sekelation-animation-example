OUT_NAME = "anim"

CXX = g++
CXXFLAGS = -std=c++17 -g -Wall -Wextra

CC = gcc
CCFLAGS = -g -Wall -Wextra

CXXFLAGS += `pkg-config --cflags assimp`
CXXFLAGS += `pkg-config --cflags glfw3`

CCFLAGS += `pkg-config --cflags assimp`
CCFLAGS += `pkg-config --cflags glfw3`

LIBS = "-ldl"
LIBS += `pkg-config --libs assimp`
LIBS += `pkg-config --libs gl`
LIBS += `pkg-config --libs glfw3`

all: main.o skinned-mesh.o texture.o glad.o stb_image.o
	$(CXX) -o $(OUT_NAME) $(CXXFLAGS) $(LIBS) \
  main.o \
  skinned-mesh.o \
  texture.o glad.o \
  stb_image.o

main.o: main.cpp
	$(CXX) $(CXXFLAGS) -c main.cpp

skinned-mesh.o: skinned-mesh.cpp skinned-mesh.hpp
	$(CXX) $(CXXFLAGS) -c skinned-mesh.cpp

texture.o: texture.cpp texture.hpp
	$(CXX) $(CXXFLAGS) -c texture.cpp

glad.o: glad.c glad.h khrplatform.h
	$(CC) $(CCFLAGS) -c glad.c

stb_image.o: stb_image.c stb_image.h
	$(CC) $(CCFLAGS) -c stb_image.c

clean:
	rm *.o $(OUT_NAME)
